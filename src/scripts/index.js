const burger = document.querySelector('.burger')
const menu = document.querySelector('.menu')
const menuItem = document.querySelectorAll('.menu__item')
const burgerSpan = document.getElementsByTagName('span')


burger.addEventListener('click', () => {
    burger.classList.toggle('active')
    menu.classList.toggle('active')
    
})

menu.addEventListener('click', (event) => {
    event.stopPropagation();
});

function toggleMenu() { 
    menu.classList.toggle('active'); 
    burger.classList.toggle('active');
};

document.addEventListener('click', (event) => {
   
    let menuIsActive = burger.classList.contains('active');

    let target = event.target;
    let clickedOnBtnMenu = target == burger || burger.contains(target);
    let clickedOnMenu = target == menu || menu.contains(target);
    
    if (menuIsActive &&  !clickedOnMenu && !clickedOnBtnMenu) {
        toggleMenu();
    }
});